$(document).ready(function () {
    // Packages table open/close
    $("#table1").on('click', function () {
        $("#t1").toggleClass('show-tb');
        if($("#t1").hasClass('show-tb')){
            $("#table1 img").attr("src","img/arrow-up.svg");
        } else {
            $("#table1 img").attr("src","img/arrow-grey.svg");
        }
    });
    $("#table2").on('click', function () {
        $("#t2").toggleClass('show-tb');
        if($("#t2").hasClass('show-tb')){
            $("#table2 img").attr("src","img/arrow-up.svg");
        } else {
            $("#table2 img").attr("src","img/arrow-grey.svg");
        }
    });
    $("#table3").on('click', function () {
        $("#t3").toggleClass('show-tb');
        if($("#t3").hasClass('show-tb')){
            $("#table3 img").attr("src","img/arrow-up.svg");
        } else {
            $("#table3 img").attr("src","img/arrow-grey.svg");
        }
    });
    $("#table1m").on('click', function () {
        $("#t1m").toggleClass('show-tb');
        if($("#t1m").hasClass('show-tb')){
            $("#table1m img").attr("src","img/arrow-up.svg");
        } else {
            $("#table1m img").attr("src","img/arrow-grey.svg");
        }
    });
    $("#table2m").on('click', function () {
        $("#t2m").toggleClass('show-tb');
        if($("#t2m").hasClass('show-tb')){
            $("#table2m img").attr("src","img/arrow-up.svg");
        } else {
            $("#table2m img").attr("src","img/arrow-grey.svg");
        }
    });
    $("#table3m").on('click', function () {
        $("#t3m").toggleClass('show-tb');
        if($("#t3m").hasClass('show-tb')){
            $("#table3m img").attr("src","img/arrow-up.svg");
        } else {
            $("#table3m img").attr("src","img/arrow-grey.svg");
        }
    });

    // Add smooth scrolling to all links
    $(".navigation a").on('click', function (event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;
            var offset = this.dataset.offset;
            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - offset // Means Less header height
            }, 800);
        } // End if
    });

    // Add smooth scrolling to all links
    $(".news-page ul li a").on('click', function (event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;
            var offset = this.dataset.offset;
            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            // $('html, body').animate({
            //     scrollTop: $(hash).offset().top - 10
            // }, 1200, function () {
            //
            //     // Add hash (#) to URL when done scrolling (default click behavior)
            //     window.location.hash = hash;
            // });

            $('html, body').animate({
                scrollTop: $(hash).offset().top - offset // Means Less header height
            }, 800);
            console.log($(this).parent());
            $('.news-page .slider_slick_filter li').removeClass('active');
            $(this).parent().addClass('active');
        } // End if
    });


    $('.search-btn').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.search-page').toggle();
    });

    $('.brands_slider_slick.gallery_slider_slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        prevArrow: $('.brands-el-left'),
        nextArrow: $('.brands-el-right'),
        autoplay: true,
        autoplaySpeed: 35000
    });
});