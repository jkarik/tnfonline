$(document).ready(function () {
    resizeSiteHeaderPlaceholder();

    $(window).on("load", function () {
        // ***************** Автоскролл горизонтального слайдера
        var horizontal_slider_content = $(".scrollbar_box"), autoScrollTimer = 60000, autoScrollTimerAdjust, autoScroll;
        horizontal_slider_content.mCustomScrollbar({
            axis: "x", // horizontal scrollbar
            mouseWheel: {enable: false},
            documentTouchScroll: true,
            moveDragger: true,
            callbacks: {
                whileScrolling: function () {
                    autoScrollTimerAdjust = autoScrollTimer * this.mcs.rightPct / 100;
                },
                onScroll: function () {
                    if ($(this).data("mCS").trigger === "internal") {
                        AutoScrollOff();
                    }
                }
            }
        });
        horizontal_slider_content.addClass("auto-scrolling-on auto-scrolling-to-right");
        AutoScrollOn("right");
        horizontal_slider_content.mouseenter(function (e) {
            AutoScrollOff();
            e.preventDefault();
            horizontal_slider_content.addClass('auto-scrolling-off')
        });
        horizontal_slider_content.mouseleave(function () {
            if (horizontal_slider_content.hasClass("auto-scrolling-off")) {
                if (horizontal_slider_content.hasClass("auto-scrolling-to-left")) {
                    horizontal_slider_content.addClass('auto-scrolling-on').removeClass('auto-scrolling-off');
                    AutoScrollOn("left")
                } else {
                    horizontal_slider_content.addClass('auto-scrolling-on').removeClass('auto-scrolling-off');
                    AutoScrollOn("right");
                }
            }
        });
        horizontal_slider_content.click(function (e) {
            e.preventDefault();
            if (horizontal_slider_content.hasClass("auto-scrolling-on")) {
                if (horizontal_slider_content.hasClass("auto-scrolling-to-left")) {
                    AutoScrollOff();
                    horizontal_slider_content.mCustomScrollbar("stop");
                    AutoScrollOn("left")
                } else {
                    AutoScrollOff();
                    horizontal_slider_content.mCustomScrollbar("stop");
                    AutoScrollOn("right");
                }
            }
        });

        function AutoScrollOn(to, timer) {
            return false;  // Отключаем автоскролл
            if (!timer) {
                timer = autoScrollTimer;
            }
            horizontal_slider_content.addClass("auto-scrolling-on").mCustomScrollbar("scrollTo", to, {
                scrollInertia: timer,
                scrollEasing: "linear"
            });
            autoScroll = setTimeout(function () {
                if (horizontal_slider_content.hasClass("auto-scrolling-to-left")) {
                    horizontal_slider_content.mCustomScrollbar("stop");
                    AutoScrollOn("right");
                    horizontal_slider_content.removeClass("auto-scrolling-to-left").addClass("auto-scrolling-to-right");
                } else {
                    horizontal_slider_content.mCustomScrollbar("stop");
                    AutoScrollOn("left")
                    horizontal_slider_content.removeClass("auto-scrolling-to-right").addClass("auto-scrolling-to-left");
                }
            }, timer);
        }

        function AutoScrollOff() {
            clearTimeout(autoScroll);
            horizontal_slider_content.removeClass("auto-scrolling-on").mCustomScrollbar("stop");
        }

        // ***************** END Автоскролл горизонтального слайдера
    });

    $('.formats-card').hover(function () {
        let width = $(this).data('width');
        $('.f_indicator_wrap').css('width', width + '%');
    }, function () {
        $('.f_indicator_wrap').css('width', '100%');
    });
    $('.brands_slider_slick.partners_slider_slick').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: true,
        dots: true,
        prevArrow: $('.brands-el-left'),
        nextArrow: $('.brands-el-right'),
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    arrows: false,
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.partners-slider .slider_slick_filter a').on('click', function () {
        $(".brands_slider_slick.partners_slider_slick").slick('slickUnfilter');
        c = '.' + $(this).data('type');
        $('.brands_slider_slick.partners_slider_slick').slick('slickFilter', c);
        $('.partners-slider .slider_slick_filter li').removeClass('active');
        $(this).parent().addClass('active');
    });

    $('.faq-question').on('click', function () {
        $('.faq-question').removeClass('active');
        $(this).addClass('active');
        $('.faq-answer').css('display', 'none');
        console.log($(this).data('id'));
        $('.faq-answer' + $(this).data('id')).css('display', 'block');
    });

    $('.brands_slider_slick.smi_slider').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: true,
        dots: true,
        prevArrow: $('.smi-el-left'),
        nextArrow: $('.smi-el-right'),
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    arrows: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }

            },
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }]
    });
    $('.org_slider_slick').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 500,
                settings: {
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    $('.brands_slider_slick.speakers_slider_slick').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: true,
        prevArrow: $('.speakers-el-left'),
        nextArrow: $('.speakers-el-right'),
        autoplay: false,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }

            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }]
    });
    $('.news-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 10000,
        infinite: false,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $(".news-slider").slick('slickUnfilter');
    $('.news-slider').slick('slickFilter', '.news-all');

    $('.news-list .slider_slick_filter a').on('click', function () {
        $(".news-slider").slick('slickUnfilter');
        c = '.' + $(this).data('type');
        $('.news-slider').slick('slickFilter', c);
        $('.news-slider .slider_slick_filter li').removeClass('active');
        $(this).parent().addClass('active');
    });

    $('.brands_slider_slick.reviews').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: true,
        dots: true,
        prevArrow: $('.reviews-el-left'),
        nextArrow: $('.reviews-el-right'),
        autoplay: false,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 666,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $('.brands_slider_slick.advantage').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: $('.advantage-el-left'),
        nextArrow: $('.advantage-el-right'),
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000
    });
    $('.brands_slider_slick.advantage-mob').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000
    });

    $('.tech-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        autoplay: false,
        autoplaySpeed: 5000
    });

    $('.vystavka-slider-for.main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: $('.vystavka-el-left'),
        nextArrow: $('.vystavka-el-right'),
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000,
        asNavFor: '.vystavka-slider-nav.main'
    });
    $('.vystavka-slider-nav.main').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.vystavka-slider-for.main',
        dots: false,
        focusOnSelect: true
    });

    $(window).resize(function () {
        resizeSiteHeaderPlaceholder();
    });

    $('.indicator').on('click', function (event) {
        event.preventDefault();

        const that = $(this);
        const img = $('.person_js');
        let personID = that.data('person');

        $('.indicator').removeClass('active_indicator');
        that.addClass('active_indicator');

        img.hide();
        $('[data-content="' + personID + '"]').fadeIn('fast');

    });

    $('[data-imgShow="1"]').addClass('img_check_show');


    /**
     * Переключатель стендов выставки
     */
    $('.ex-card__vars').find('a').on('click', function (e) {
        e.preventDefault();

        const that = $(this);
        const thatData = that.data('img');
        if (thatData > 0) {
            // $('.img_check').stop().removeClass('img_check_show');
            that.closest(".ex-card").find(".ex-card__gallery").find("img").addClass('img-hidden');
            that.parent().parent().find('a').removeClass('active_a');
            that.parent().parent().find('p').removeClass('active_a');
        }
        that.addClass('active_a');
        that.parent().find('p').addClass('active_a');
        $('[data-img-show="' + thatData + '"]').removeClass('img-hidden');
    });

    /**
     * Автоматически переключаем стенды
     */
    var changeStandInterval = setInterval(function () {
        $('.ex-card__vars').each(function () {
            var next = $(this).find('.active_a').parent().next().find('a');
            if (!next.length) {
                next = $(this).parent().parent().find('a').first();
            }
            next.click()
        })
    }, 5000);

    function equalHeight(a, b) {
        let a_height = a.height();

        b.css('height', a_height);
    }

    equalHeight($('.text_h'), $('.img_h'));
    equalHeight($('.text_h_v'), $('.img_h_v'));
    equalHeight($('.slick-slider'), $('.teaser-item'));
    equalHeight($('.ci_repl'), $('.ci_img'));


    $(window).resize(function () {
        equalHeight($('.text_h'), $('.img_h'));
        equalHeight($('.slick-slider'), $('.teaser-item'));
        equalHeight($('.ci_repl'), $('.ci_img'));
        equalHeight($('.text_h_v'), $('.img_h_v'));
    });

    $('.burger').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.mobile_menu').show();
    });

    $('.mobile_menu').on('click', function (e) {
        e.stopPropagation();
    });

    $('body').on('click', function (e) {
        $('.mobile_menu').hide();
    });

    $('.close-mobile-m').on('click', function (e) {
        e.preventDefault();
        $('.mobile_menu').hide();
    });

    $('.reg-right-section input').on('input', function () {
        if ($(this).val() != "") {
            $(this).siblings('label').css('display', 'block');
            $(this).css('padding-top', '19px');
        } else {
            $(this).siblings('label').css('display', 'none');
            $(this).css('padding-top', '0');
        }
    });
    $('.reg-right-section textarea').on('input', function () {
        if ($(this).val() != "") {
            $(this).siblings('label').css('display', 'block');
        } else {
            $(this).siblings('label').css('display', 'none');
        }
    });
    $(document).on('click', '[data-vystavka-pack-id]', function (e) {
        $('#' + $(this).data('vystavka-pack-id')).modal('show');
        $(window).resize();

        $('.vystavka-slider-for.' + $(this).data('vystavka-pack-id')).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            autoplay: false,
            autoplaySpeed: 5000,
            asNavFor: '.vystavka-slider-nav.' + $(this).data('vystavka-pack-id')
        });
        $('.vystavka-slider-nav.' + $(this).data('vystavka-pack-id')).slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            infinite: true,
            asNavFor: '.vystavka-slider-for.' + $(this).data('vystavka-pack-id'),
            dots: false,
            focusOnSelect: true
        });
    });
    $(window).resize();
});

function newsSlider(item) {
    $('.news-slider').css('display', 'none');
    $('.news-' + item).css('display', 'block');
}

function resizeSiteHeaderPlaceholder() {
    if ($('.site-header').css('position') == 'fixed') {
        var header_height = $('.site-header').height()
        $('.site-header-placeholder').height(header_height)
    }
    else {
        $('.site-header-placeholder').height('0')
    }
}

// Открываем полное описание форматов участия
$(document).on('click', '.formats-cards .button-description a', function (e) {
    if ($(".formats-cards .button-description a").hasClass("open")) {
        $(".formats-card-long-text").css("display", "none");
        $(".formats-cards .button-description a svg.closed").css("display", "inline-block");
        $(".formats-cards .button-description a svg.opened").css("display", "none");
        $(".formats-cards .button-description a").removeClass("open");
        $(".formats-cards .button-description a").parent().css("border-top", "none");
        $(".formats-cards .button-description a").parent().css("border-bottom", "1px solid #DCEFFE");
        $(".formats-cards .button-description a").parent().css("padding", "2.143rem 0");
        $(".formats-cards .button-description a span").text("Читать описание");
    } else {
        $(".formats-card-long-text").css("display", "block");
        $(".formats-cards .button-description a svg.closed").css("display", "none");
        $(".formats-cards .button-description a svg.opened").css("display", "inline-block");
        $(".formats-cards .button-description a").addClass("open");
        $(".formats-cards .button-description a").parent().css("border-bottom", "none");
        $(".formats-cards .button-description a").parent().css("border-top", "1px solid #DCEFFE");
        $(".formats-cards .button-description a").parent().css("padding", "2.143rem 0 0");
        $(".formats-cards .button-description a span").text("Скрыть описание");
    }
});
// Открываем полное описание пакетов участия в выставке
$(document).on('click', '.participation-cards .button-description a', function (e) {
    if ($(".participation-cards .button-description a").hasClass("open")) {
        $(".participation-cards-long-text").css("display", "none");
        $(".participation-cards .button-description a svg.closed").css("display", "inline-block");
        $(".participation-cards .button-description a svg.opened").css("display", "none");
        $(".participation-cards .button-description a").removeClass("open");
        $(".participation-cards .button-description a").parent().css("border-top", "none");
        $(".participation-cards .button-description a").parent().css("border-bottom", "1px solid #DCEFFE");
        $(".participation-cards .button-description a span").text("Посмотреть полное описание");
    } else {
        $(".participation-cards-long-text").css("display", "block");
        $(".participation-cards .button-description a svg.closed").css("display", "none");
        $(".participation-cards .button-description a svg.opened").css("display", "inline-block");
        $(".participation-cards .button-description a").addClass("open");
        $(".participation-cards .button-description a").parent().css("border-bottom", "none");
        $(".participation-cards .button-description a").parent().css("border-top", "1px solid #DCEFFE");
        $(".participation-cards .button-description a span").text("Скрыть полное описание");
    }
});

function emailSubscribe(form) {
    $.ajax({
        url: '/ajax/mail_subscribe.php',
        type: 'post',
        data: form,
        success: function (data) {
            $('#subscribeResultModal .modal-header .modal-title span').text(data);
            $('#subscribeResultModal').modal('show');
        }
    });
    return false;
}
