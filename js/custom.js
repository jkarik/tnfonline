var currentMainSlide = 1;
var currentMSlide = 1;

function init() {
	$('.window-height').height($(window).height() - 90);
}

function nextMainSlide() {
	// закоменчено тк один слайд

	currentMainSlide++;
	if (currentMainSlide > 4) currentMainSlide = 1;
	$('.main-top-slider').hide();

	$('#mainTopSlide' + currentMainSlide).fadeIn('normal');
	$('.top-slider-pagination a').removeClass("active");
	$('#tsp' + currentMainSlide).addClass("active");
}

function nextMSlide() {
	currentMSlide++;
	if (currentMSlide > $(".mslide>div").length) currentMSlide = 1;

	$('.mslide>div').hide();
	$('#mSlide' + currentMSlide).fadeIn('normal');
	$('.mslide-nav a').removeClass('active');
	$('a[data-slide=mSlide' + currentMSlide + ']').addClass('active');
}

$(function () {
	init();

	// AOS.init();

	new ModalVideo('.js-modal-video');
	$('.modal .close_btn').on("click", function (event) {
		$('.modal').hide();
		return false;
	});

	
	// Подстраиваем вёрстку под высоту панели Битрикс и высоту меню
	function adapt_layout_to_bx_panel(){
		if(document.getElementById("bx-panel")) {
			$(".header").add(".type-body").add(".body-header").add(".type-body").css("margin-top", $("#bx-panel").height());
		}
		if(document.getElementById("body-header") !== null){
			// $(".body-header").css('margin-top', $(".header").height() + 20);
			$(".body-header").css('margin-top', $(".header").height());
		}else{
			// $(".type-body").css('margin-top', $(".header").height() + 20);
			$(".type-body").css('margin-top', $(".header").height());
			
		}
	}
	
	if($("#bx-panel").height()){
		adapt_layout_to_bx_panel();
	}
	$("#bx-panel-expander").add("#bx-panel-hider").on('click', function(){
		adapt_layout_to_bx_panel()
	})
	
	adapt_layout_to_bx_panel();

	$('.type-body').css("min-height", $(window).height() - $('.footer').height() - $('.header').height() - 100 - $('.under-header').height() - $('#panel').height() + "px");
	// jQuery('.flipInX-animation').addClass("opacity0").viewportChecker({
	// 	classToAdd: 'opacity1 animated flipInX', // Class to add to the elements when they are visible
	// 	offset: 100
	// });
	// jQuery('.fadeInDown-animation').addClass("opacity0").viewportChecker({
	// 	classToAdd: 'opacity1 animated fadeInDown', // Class to add to the elements when they are visible
	// 	offset: 100
	// });
	// jQuery('.bounceInUp-animation').addClass("opacity0").viewportChecker({
	// 	classToAdd: 'opacity1 animated bounceInUp', // Class to add to the elements when they are visible
	// 	offset: 100
	// });
	// jQuery('.bounceInLeft-animation').addClass("opacity0").viewportChecker({
	// 	classToAdd: 'opacity1 animated bounceInLeft', // Class to add to the elements when they are visible
	// 	offset: 100
	// });

	// $('.stat-icon-digit').each(function(){
	// 	var obj = this;
	// 	$({
	// 		n: parseInt($(obj).html())/2
	// 	}).animate({
	// 	    n: parseInt($(obj).html())
	// 	}, {
	// 		duration: 1000,
	// 		step: function (a) {
	// 			$(obj).html(a | 0);
	// 		}
	// 	})
	// });
	$(".fancybox").fancybox();
	$(".fancybox-iframe").fancybox({
		type: "iframe"
	});


	$('.packet-radio').click(function () {
		var id = $(this).attr('id').replace("packet", "");
		$('.packet-desc').hide();
		$('#packetDesc' + id).fadeIn('normal');
	});
	$('.packet-label-flat').click(function () {
		$('.packet-radio-flat').removeAttr("checked");
		$('#' + $(this).attr('for')).attr("checked", "checked");
		console.log('#' + $(this).attr('for'));
		$('.packet-label-flat').removeClass("active");
		$(this).addClass("active");

		var id = $('#' + $(this).attr('for')).attr('id').replace("packet", "");
		$('.packet-desc').hide();
		$('#packetDesc' + id).fadeIn('normal');

		$('.reg-steps .step2').show();
		$('.reg-steps .step3').show();
		$('#stepNext').show();
		$('#stepReg').hide();

		// Разные шаги при регистрации в зависимости от выбранного пакета
		if (id == 1) {
			$('#stepNext').hide();
			$('#stepReg').show();
			$('.reg-steps .step2').hide();
			$('.reg-steps .step3').hide();
		}
	});
	$('.participant-tabs li').click(function () {
		$('.participant-tabs li').removeClass('active');
		$(this).addClass('active');
		$('.participant-tab').hide();
		// debugger
		$($(this).data('tab')).fadeIn('normal');
		$($(this).data('label')).click();
	});
	
	$('.flexslider').flexslider({
		animation: "slide"
	});

	$('.flexslider-speakers').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		autoplay: true,
		autoplaySpeed: 3500,
		responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,

				}
			},

			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.flexslider-partners').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 6,
		autoplay: true,
		autoplaySpeed: 3500,
		responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,

				}
			},

			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});


	$('.program-slider').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	$('.icon-pdf').hover(function () { // закешировать this.find.img.src
		if ($(this).find('img').attr('src') == "/bitrix/templates/oilgasforum/images/icon-pdf.png") $(this).find('img').attr('src', '/bitrix/templates/oilgasforum/images/icon-pdf-h.png');
		if ($(this).find('img').attr('src') == "/bitrix/templates/oilgasforum/images/icon-docx.png") $(this).find('img').attr('src', '/bitrix/templates/oilgasforum/images/icon-docx-h.png');
	}, function () {
		if ($(this).find('img').attr('src') == "/bitrix/templates/oilgasforum/images/icon-pdf-h.png") $(this).find('img').attr('src', '/bitrix/templates/oilgasforum/images/icon-pdf.png');
		if ($(this).find('img').attr('src') == "/bitrix/templates/oilgasforum/images/icon-docx-h.png") $(this).find('img').attr('src', '/bitrix/templates/oilgasforum/images/icon-docx.png');
	});
	// setInterval(nextMainSlide, 7000);
	$('.top-slider-pagination a').click(function () {
		currentMainSlide = parseInt($(this).attr("id").replace("tsp", ""));

		$('.main-top-slider').hide();
		$('#mainTopSlide' + currentMainSlide).fadeIn('normal');
		$('.top-slider-pagination a').removeClass("active");
		$('#tsp' + currentMainSlide).addClass("active");

		return false;
	});
	var mSlideInterval = setInterval(nextMSlide, 6000);
	$('.mslide-nav a').click(function () {
		clearInterval(mSlideInterval);
		var slide = $(this).data('slide');
		$('.mslide>div').hide();
		$('#' + slide).fadeIn('normal');
		$('.mslide-nav a').removeClass('active');
		$(this).addClass('active');
		return false;
	});
	$('.btn-reg-main').click(function () {
		var scroll_el = $(this).attr('href');
		if ($(scroll_el).length != 0) {
			$('html, body').animate({
				scrollTop: $(scroll_el).offset().top - 100
			}, 500);
		}
		return false;
	});
	$('.h2-tabs li a').click(function () {
		var parent = $(this).parent().parent();
		var div = $(parent).data("div");

		$('.h2-tabs li').removeClass('active');
		$(parent).addClass('active');

		$('.h2-tab').hide();
		$('#' + div).fadeIn();
		return false;
	});
});

// vS
$(function () {
	$('input[name="PHONE"], input[name="user_phone"]').inputmask("+9{1} 9{3} 9{3} 9{2} 9{2}");
	$('input[name="CONTACT_PHONE"], input[name="user_phone"]').inputmask("+9{1} 9{3} 9{3} 9{2} 9{2}");
	$('input[name="BIRTHDATE"], input[name="user_phone"]').inputmask("9{2}.9{2}.9{4}");

	$('a[href="#o-forume"]').on('click', function () {
		$('.stat-icon-digit-main').each(function () {
			var obj = this;
			$({
				n: parseInt($(obj).html()) / 2
			}).animate({
				n: parseInt($(obj).html())
			}, {
				duration: 1000,
				step: function (a) {
					$(obj).html(a | 0);
				}
			})
		});
	});

	// top slider control
	$('.top_slider_control a').on('click', function (e) {
		e.preventDefault();
		let numActiveSlide = $('.top-slider-pagination').find('.active').attr('href').match(/\d+/);
		let activeSlideNum = parseInt(numActiveSlide);
		console.log(activeSlideNum);
		if ($(this).hasClass('top_slider_l_arrow')) {
			if (activeSlideNum == 1) {
				$('#tsp4').click();
			} else {
				$('#tsp' + (activeSlideNum - 1) + '').click();
			}
		} else if ($(this).hasClass('top_slider_r_arrow')) {
			if (activeSlideNum == 4) {
				$('#tsp1').click();
			} else {
				$('#tsp' + (activeSlideNum + 1) + '').click();
			}
		}
	});
	if($("#main_map").length){
		ymaps.ready(init1);
	}
});

// new ModalVideo('.js-modal-video');
// $('.modal .close_btn').on('click', function (event) {
// 	$('.modal').hide();
// 	return false;
// });

// ymaps.ready(init1);
var myMap,
	myPlacemark,
	center = [57.137461, 65.566514];

function init1() {
	myMap = new ymaps.Map("main_map", {
		center: center,
		zoom: 16,
		controls: []
	});

	mark_1 = new ymaps.Placemark([57.137461, 65.566514], {
		hintContent: 'Западно-Сибирский инновационный центр, Тюмень, улица Республики, 142'
	}, {
		iconLayout: 'default#image',
		iconImageHref: '/bitrix/images/m1.png',
		iconImageSize: [106, 113],
		iconImageOffset: [-53, -102]
	});

	myMap.geoObjects.add(mark_1);
}
$(document).ready(function () {
	var show = true;
	var countbox = ".main-digits .col-md-6";
	$(window).on("scroll load resize", function () {
		if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
		var w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
		if($(countbox).length){
			// var var var var var var var var var var - ?
			var e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
			var w_height = $(window).height(); // Высота окна браузера
			var d_height = $(document).height(); // Высота всего документа
			var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
			if (w_top >= e_top - w_height + e_height) {
				$(".spincrement").spincrement({
					thousandSeparator: " ",
					duration: 1500
				});
				show = false;
			}
		}
	});

	$('.techDaysSlider').slick({
		// autoplay: true,
		// autoplaySpeed: 3000,
		dots: true,
		slidesToShow: 1,
  		slidesToScroll: 1,
	});

});

$(window).on('load', function() {
	var header_height = $(".site-header").height();
	document.header_height = header_height
	$(".teaser").add(".teaser__content-container").css({
		// 'top': 'calc(' + header_height + 'px + 1rem)',
		'min-height': 'calc(100vh - ' + header_height + 'px)'
	});

	$('[data-toggle="popover"]').popover();
	
	$('#download-project-of-program').on('click', function(){
		$('.modal .close_btn').click();
		
	})

	// Выбираем нужный тариф на странице форматов участия
	if ($('.formats-area').length > 0) { //правильнее будет заменить на конструкцию switch case
		if (window.location.hash == "#guest") {
			$('[data-code=".tab-guest"]').click();
			// setStep(1);
		}
		if (window.location.hash == "#guest-desc") {
			$('[data-code=".tab-guest"]').click();
		}
		if (window.location.hash == "#participant") {
			$('[data-code=".tab-participant"]').click();
			// setStep(1);
		}
		if (window.location.hash == "#participant-desc") {
			$('[data-code=".tab-participant"]').click();
		}
		if (window.location.hash == "#delegate") {
			$('[data-code="delegate"]').click();
			setTimeout(() => {
				$('[data-code="delegate"]').click();
			}, 0.5);
			setTimeout(() => {
				$('[data-code="delegate"]').click();
			}, 1);
			// setStep(1);
		}
		if (window.location.hash == "#delegate-desc") {
			$('[data-code=".tab-delegate"]').click();
		}
		if (window.location.hash == "#premium") {
			$('[data-code="premium"]').click();
			setTimeout(() => {
				$('[data-code="premium"]').click();
			}, 0.5);
			setTimeout(() => {
				$('[data-code="premium"]').click();
			}, 1);
			// setStep(1);
		}
		if (window.location.hash == "#premium-desc") {
			$('[data-code=".tab-premium"]').click();
		}
		if (window.location.hash == "#speaker") {
			$('[data-code="speaker"]').click();
			setTimeout(() => {
				$('[data-code="speaker"]').click();
			}, 0.5);
			setTimeout(() => {
				$('[data-code="speaker"]').click();
			}, 1);
			// setStep(1);
		}
		if (window.location.hash == "#speaker-desc") {
			$('[data-code=".tab-speaker"]').click();
		}
		if (window.location.hash == "#business") {
			$('[data-code="#business"]').click();
			setTimeout(() => {
				$('[data-code="business"]').click();
			}, 0.5);
			setTimeout(() => {
				$('[data-code="business"]').click();
			}, 1);
			// setStep(1);
		}
	}


	// $('.modal .close_btn').on('click', function (event) {
	// 	$('.modal').hide();
	// 	return false;
	// });

});

// Ленивая загрузка видео
document.addEventListener("DOMContentLoaded", function () {
	var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));

	if ("IntersectionObserver" in window) {
		var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
			entries.forEach(function (video) {
				if (video.isIntersecting) {
					for (var source in video.target.children) {
						var videoSource = video.target.children[source];
						if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
							videoSource.src = videoSource.dataset.src;
						}
					}

					video.target.load();
					video.target.classList.remove("lazy");
					lazyVideoObserver.unobserve(video.target);
				}
			});
		});

		lazyVideos.forEach(function (lazyVideo) {
			lazyVideoObserver.observe(lazyVideo);
		});
	}
});

function toggl_block_visibitity(block_selector) {
	$(block_selector).fadeToggle();
}
